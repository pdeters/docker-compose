Docker Compose
==============

Prerequisites
-------------

Download [Docker Desktop](https://www.docker.com/products/docker-desktop) for Mac or Windows. [Docker Compose](https://docs.docker.com/compose) will be automatically installed. On Linux, make sure you have the latest version of [Compose](https://docs.docker.com/compose/install/).

MySQL 5
-------
To create a MySQL 5 container, and load a nightly sql backup.
```console
$ docker-compose up
$ mysql -h localhost -P 3306 --protocol=tcp -u root -p tbuilder_dev < ./nightly.sql
```

To destroy the container.
```console
$ docker-compose down -v
```
